﻿using Microsoft.Win32;
using PlanetsDatabase.Database;
using PlanetsDatabase.Models;
using PlanetsDatabase.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PlanetsDatabase {
    /// <summary>
    /// Логика взаимодействия для PlanetRecordWindow.xaml
    /// </summary>
    public partial class PlanetRecordWindow : Window {

        public Planet Planet {
            get;
            private set;
        }

        public List<PropertyValuePair> PropertyValuePairs {
            get;
            private set;
        }

        DatabaseContext dbContext;

        private bool _iconChanged;

        public PlanetRecordWindow(Planet planet) {
            InitializeComponent();
            dbContext = new DatabaseContext();
            Planet = planet;
            DataContext = Planet;
            _iconChanged = false;
            ShowPropertyValuesForCurrentPlanet();
            LoadIconForPlanet();
        }

        private void DeletePlanetValueButton_Click(object sender, RoutedEventArgs e) {
            PropertyValuePair deletedPair = ((FrameworkElement)sender).DataContext as PropertyValuePair;
            if ( deletedPair != null ) {
                string errorMessage = "";
                if ( !dbContext.DeletePlanetProperty(Planet, deletedPair, out errorMessage) ) {
                    MessageBox.Show("Can not delete property with value due to an error: " + errorMessage);
                }
                ShowPropertyValuesForCurrentPlanet();
            }
        }

        private void LoadIconForPlanet() {
            if ( Planet.Icon != null && Planet.Icon.Length != 0 ) {
                PlanetIcon.Source = ImageUtilConverter.ConvertBytesToBitmap(Planet.Icon);
                DeleteIconButton.Visibility = System.Windows.Visibility.Visible;
            }
            else {
                PlanetIcon.Source = ImageUtilConverter.BitmapFromUri(new Uri("pack://application:,,,/Images/planetDefault.jpg"));
                DeleteIconButton.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void ShowPropertyValuesForCurrentPlanet() {
            string errorMessage = "";
            PropertyValuePairs = dbContext.LoadPropertiesAndValuesForPlanet(Planet, out errorMessage);
            if(!string.IsNullOrEmpty(errorMessage) ) {
                MessageBox.Show("Can not get properties with value due to an error: " + errorMessage);
            }
            else {
                PlanetPropertiesDataGrid.ItemsSource = null;
                PlanetPropertiesDataGrid.ItemsSource = PropertyValuePairs;
            }
        }

        private void EditPlanetValueButton_Click(object sender, RoutedEventArgs e) {
            PropertyValuePair selectedProperty = ((FrameworkElement)sender).DataContext as PropertyValuePair;
            if ( selectedProperty != null ) {
                EditPropertyPlanetWindow editPropertyWindow = new EditPropertyPlanetWindow(Planet, selectedProperty);
                if ( editPropertyWindow.ShowDialog() == true ) {
                    ShowPropertyValuesForCurrentPlanet();
                }
            }
        }

        private void AddNewPropertyButton_Click(object sender, RoutedEventArgs e) {
            AddPropertyPlanetWindow addPropertyWindow = new AddPropertyPlanetWindow(Planet);
            if ( addPropertyWindow.ShowDialog() == true ) {
                ShowPropertyValuesForCurrentPlanet();
            }
        }

        private void PlanetIconButton_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog imageLoadDialog = new OpenFileDialog();
            imageLoadDialog.Title = "Select a picture";
            imageLoadDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if ( imageLoadDialog.ShowDialog() == true ) {
                _iconChanged = true;
                var imagePath = new Uri(imageLoadDialog.FileName);
                ImageSource imageSource = ImageUtilConverter.BitmapFromUri(imagePath);
                PlanetIcon.Source = imageSource;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if ( _iconChanged ) {
                string errorMessage = "";
                BitmapImage source = PlanetIcon.Source as BitmapImage;
                ImageUtilConverter converter = new ImageUtilConverter();
                Byte[] icon = File.ReadAllBytes(((BitmapImage)PlanetIcon.Source).UriSource.AbsolutePath);
                if ( !dbContext.AddIconToPlanet(icon, Planet, out errorMessage) ) {
                    MessageBox.Show("Can not add icon to a planet due to an error: " + errorMessage);
                }
            }
        }

        private void DeleteIconButton_Click(object sender, RoutedEventArgs e) {
            DeleteIconButton.Visibility = System.Windows.Visibility.Hidden;
            string errorMessage = "";
            if ( !dbContext.DeleteIconForPlanet( Planet, out errorMessage) ) {
                MessageBox.Show("Can not delete icon for a planet due to an error: " + errorMessage);
            }
            LoadIconForPlanet();
        }
    }
}
