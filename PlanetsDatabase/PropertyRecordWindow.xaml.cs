﻿using PlanetsDatabase.Database;
using PlanetsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlanetsDatabase {
    /// <summary>
    /// Логика взаимодействия для PropertyRecordWindow.xaml
    /// </summary>
    public partial class PropertyRecordWindow : Window {

		DatabaseContext dbContext;

		public Property Property { get; private set; }

        public PropertyRecordWindow(Property property) {
            InitializeComponent();
			dbContext = new DatabaseContext();
            Property = property;
            DataContext = Property;
            ShowValuesForCurrentProperty();
        }

        private void ShowValuesForCurrentProperty() {
            string errorMessage = "";
            if ( dbContext.LoadValuesForProperty(Property, out errorMessage) ) {
                PropertyValuesDataGrid.ItemsSource = null;
                PropertyValuesDataGrid.ItemsSource = Property.PossibleValues;
            }
            else {
                MessageBox.Show("Can not get values for current property due to error: " + errorMessage);
            }
        }

        private void SaveValueButton_Click(object sender, RoutedEventArgs e) {
            
            Value updatedVal = ((FrameworkElement)sender).DataContext as Value;
            string errorMessage = "";
            if ( updatedVal != null ) {

                if ( updatedVal.Exists() ) {
                    if ( !dbContext.SaveValue(updatedVal, out errorMessage) ) {
                        MessageBox.Show("Value can not be saved due to error: " + errorMessage);
                    }
                }
                else {
                    if ( !dbContext.AddValueToProperty(updatedVal, Property, out errorMessage) ) {
                        MessageBox.Show("Value can not be addded due to error: " + errorMessage);
                    }
                }
                ShowValuesForCurrentProperty();
            } 
            else {
                MessageBox.Show("Can not create empty Value!");
            }
        }

        private void DeleteValueButton_Click(object sender, RoutedEventArgs e) {
            Value deletedVal = ((FrameworkElement)sender).DataContext as Value;
            if ( deletedVal != null ) {
                string errorMessage = "";
                if ( !dbContext.DeleteValue(deletedVal, out errorMessage) ) {
                    MessageBox.Show("Value can not be deleted due to error: " + errorMessage);
                }
                ShowValuesForCurrentProperty();
            }
        }
	}
}
