﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PlanetsDatabase.Utils {
    public class ImageUtilConverter {

        public static BitmapImage ConvertBytesToBitmap(byte[] bytes) {

            MemoryStream stream = new MemoryStream(bytes);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        public static ImageSource BitmapFromUri(Uri source) {
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = source;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }

        public static byte[] ConvertImageToBytes(BitmapImage image) {
            Stream stream = image.StreamSource;
            Byte[] bytes = null;
            if ( stream != null && stream.Length > 0 ) {
                using ( BinaryReader br = new BinaryReader(stream) ) {
                    bytes = br.ReadBytes((Int32)stream.Length);
                }
            }
            return bytes;
        }
    }
}
