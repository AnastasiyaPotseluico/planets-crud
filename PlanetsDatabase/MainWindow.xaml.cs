﻿using PlanetsDatabase.Database;
using PlanetsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlanetsDatabase
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		DatabaseContext dbContext;

		public MainWindow()
		{
			InitializeComponent();
			dbContext = new DatabaseContext();
		}

        private void EditPropertyButton_Click(object sender, RoutedEventArgs e) {
            var selectedProperty = PropertiesGrid.SelectedItem;
            if ( selectedProperty != null ) {
                PropertyRecordWindow editPropertyWindow = new PropertyRecordWindow((Property)selectedProperty);
                editPropertyWindow.Show();
            }
        }

        private void EditPlanetButton_Click(object sender, RoutedEventArgs e) {
            var selectedPlanet = PlanetsGrid.SelectedItem;
            if ( selectedPlanet != null ) {
                PlanetRecordWindow editPlanetWindow = new PlanetRecordWindow((Planet)selectedPlanet);
                editPlanetWindow.Closed += RefreshPlanetsTable;
                editPlanetWindow.ShowDialog();
            }
        }

        private void RefreshPlanetsTable(object sender, EventArgs e) {
            ShowPlanets();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if ( e.Source is TabControl ) {
                if ( PlanetsTabItem.IsSelected ) {
                    ShowPlanets();
                }
                else if ( PropertiesTabItem.IsSelected ) {
                    ShowProperties();
                }
                e.Handled = true;
            }
        }

        private void ShowPlanets() {
            string errorMessage = "";
            var planets = dbContext.LoadPlanets(out errorMessage);
            if ( String.IsNullOrEmpty(errorMessage) ) {
                PlanetsGrid.ItemsSource = null;
                PlanetsGrid.ItemsSource = planets;
            }
            else {
                MessageBox.Show("Can not load planets due to error: " + errorMessage);
            }
        }

        private void ShowProperties() {
            string errorMessage = "";
            var properties = dbContext.LoadProperties(out errorMessage);
            if ( String.IsNullOrEmpty(errorMessage) ) {
                PropertiesGrid.ItemsSource = null;
                PropertiesGrid.ItemsSource = properties;
            }
            else {
                MessageBox.Show("Can not load properties due to error: " + errorMessage);
            }
        }

        private void DeletePropertyButton_Click(object sender, RoutedEventArgs e) {
            Property deletedProp = ((FrameworkElement)sender).DataContext as Property;
            if ( deletedProp != null ) {
                string errorMessage = "";
                MessageBoxResult userAnswer = MessageBox.Show("Delete selected property?", "Warning", MessageBoxButton.OKCancel);
                if ( userAnswer == MessageBoxResult.OK ) {
                    if ( !dbContext.DeleteProperty(deletedProp, out errorMessage) ) {
                        MessageBox.Show("Property can not be deleted due to error: " + errorMessage);
                    }
                    ShowProperties();
                }
            }
        }

        private void SavePropertyButton_Click(object sender, RoutedEventArgs e) {
            Property updatedProp = ((FrameworkElement)sender).DataContext as Property;
            string errorMessage = "";
            if ( updatedProp!= null && !String.IsNullOrEmpty(updatedProp.Name) ) {
                if ( updatedProp.Exists() ) {
                    if ( !dbContext.SaveProperty(updatedProp, out errorMessage) ) {
                        MessageBox.Show("Property can not be saved due to error: " + errorMessage);
                    }
                }
                else {
                    if ( !dbContext.AddProperty(updatedProp, out errorMessage) ) {
                        MessageBox.Show("Property can not be added due to error: " + errorMessage);
                    }
                }
                ShowProperties();
            }
            else {
                MessageBox.Show("Can not create Property with empty name!");
            }
        }

        private void DeletePlanetButton_Click(object sender, RoutedEventArgs e) {
            Planet deletedPlanet = ((FrameworkElement)sender).DataContext as Planet;
            if ( deletedPlanet != null ) {
                string errorMessage = "";
                MessageBoxResult userAnswer = MessageBox.Show("Delete selected planet?", "Warning", MessageBoxButton.OKCancel);
                if ( userAnswer == MessageBoxResult.OK ) {
                    if ( !dbContext.DeletePlanet(deletedPlanet, out errorMessage) ) {
                        MessageBox.Show("Planet can not be deleted due to error: " + errorMessage);
                    }
                    ShowPlanets();
                }
            }
        }

        private void SavePlanetButton_Click(object sender, RoutedEventArgs e) {
            Planet updatedPlanet = ((FrameworkElement)sender).DataContext as Planet;
            string errorMessage = "";
            if ( updatedPlanet != null && !String.IsNullOrEmpty(updatedPlanet.Name)) {
                if ( updatedPlanet.Exists() ) {
                    if(!dbContext.SavePlanet(updatedPlanet, out errorMessage)) {
                        MessageBox.Show("Planet can not be saved due to error: " + errorMessage);
                    }
                }
                else {
                    if ( !dbContext.AddPlanet(updatedPlanet, out errorMessage) ) {
                        MessageBox.Show("Planet can not be added due to error: " + errorMessage);
                    }
                }
                ShowPlanets();
            }
            else {
                MessageBox.Show("Can not create Planet with empty name!");
            }
        }
	}
}
