﻿using PlanetsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PlanetsDatabase.Database {
    /// <summary>
    /// Class for database access 
    /// </summary>
    class DatabaseContext : DbContext {
        public DatabaseContext()
            : base("DefaultConnection") {
        }

        public bool AddPlanet(Planet planet, out string errorMessage) {
            string sqlQuery = String.Format("Insert into Planets (Name, Weight, Radius) values (@name, @weight, @radius)");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("name", planet.Name));
            parameters.Add(new SQLiteParameter("weight", planet.Weight));
            parameters.Add(new SQLiteParameter("radius", planet.Radius));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        public bool AddProperty(Property property, out string errorMessage) {
            string sqlQuery = String.Format("Insert into Properties (Name) values (@name)", property.Name);
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("name", property.Name));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        /// <summary>
        /// Method for adding a value into possible values of a property
        /// </summary>
        /// <param name="value">value to be added</param>
        /// <param name="property">property for adding new possible value</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>operation success</returns>
        public bool AddValueToProperty(Value value, Property property, out string errorMessage) {
            string sqlQuery = String.Format("Insert into Property_Values (Text_Value) values (@value)");
            bool execWithoutErrors;
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("value", value.Text));
            execWithoutErrors = ExecuteQuery(sqlQuery, parameters, out errorMessage);
            sqlQuery = String.Format("Insert Into Pairs_Properties_Values (Id_Property, Id_Value) values (@idProperty, (SELECT Id from Property_Values WHERE Text_Value = @value))");
            parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("idProperty", property.Id));
            parameters.Add(new SQLiteParameter("value", value.Text));
            execWithoutErrors &= ExecuteQuery(sqlQuery, parameters, out errorMessage);
            return execWithoutErrors;
        }

        /// <summary>
        /// Method for deleting property
        /// </summary>
        /// <param name="property"> property to be deleted</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>operation success</returns>
        public bool DeleteProperty(Property property, out string errorMessage) {
            string sqlQuery;
            bool execWithoutErrors = true;
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            if ( property.PossibleValues != null ) { //Values can't exist without property, deleting connected values first
                List<string> valuesIds = property.PossibleValues.Select(val => val.Id.ToString()).ToList();
                string idsForSql = String.Join(",", valuesIds);
                sqlQuery = String.Format("Delete from Property_Values Where Id in (@id)", idsForSql);
                parameters.Add(new SQLiteParameter("id", idsForSql));
                execWithoutErrors = ExecuteQuery(sqlQuery, parameters, out errorMessage);
            }
            sqlQuery = String.Format("Delete from Properties Where Id=@id");
            parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("id", property.Id));
            execWithoutErrors &= ExecuteQuery(sqlQuery, parameters, out errorMessage);
            return execWithoutErrors;
        }

        public bool DeleteValue(Value value, out string errorMessage) {
            string sqlQuery = String.Format("Delete from Property_Values Where Id=@id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("id", value.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        public bool SaveValue(Value value, out string errorMessage) {
            string sqlQuery = String.Format("Update Property_Values SET Text_Value = @value WHERE Id = @id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("value", value.Text));
            parameters.Add(new SQLiteParameter("id", value.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        public bool SaveProperty(Property property, out string errorMessage) {
            string sqlQuery = String.Format("Update Properties SET Name = @name WHERE Id = @id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("name", property.Name));
            parameters.Add(new SQLiteParameter("id", property.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        /// <summary>
        /// Method for getting all planets with connected properties and values
        /// </summary>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>list of planets</returns>
        public List<Planet> LoadPlanets(out string errorMessage) {
            var planets = new List<Planet>();
            errorMessage = "";
            using ( SQLiteConnection db = new SQLiteConnection(@"Data Source=.\Database\Planets.db") ) {
                db.Open();
                try {
                    // Here we are joining tables to get names of values and properties for each planet
                    SQLiteCommand selectCommand = new SQLiteCommand
                        (@"select Planets.Name as Name, Planets.Radius as Radius, Planets.Weight as Weight, Planets.Id as Id, 
								prop.Name as PropertyName, ifnull(prop.Id, -1) as IdProperty, vals.Text_Value as TextValue, ifnull(vals.Id, -1) as IdValue, Planets.Icon as Icon FROM Planets
								Left JOIN Planets_Properties_Values as ppv ON Planets.Id = ppv.Id_Planet
								Left JOIN Properties as prop ON ppv.Id_Property = prop.Id
								Left JOIN Property_Values as vals ON ppv.Id_Value = vals.Id", db);

                    SQLiteDataReader query = selectCommand.ExecuteReader();

                    while ( query.Read() ) {
                        Planet newPlanet = new Planet();
                        newPlanet.Name = query.GetString(0);
                        var radius = query.IsDBNull(1) ? "" : query.GetString(1);
                        newPlanet.Radius = query.IsDBNull(1) ? "" : query.GetString(1);
                        newPlanet.Weight = query.IsDBNull(2) ? "" : query.GetString(2);
                        newPlanet.Id = query.GetInt32(3);
                        newPlanet.PropertyName = query.IsDBNull(4) ? "" : query.GetString(4);
                        newPlanet.IdProperty = query.GetInt32(5);
                        newPlanet.ValueText = query.IsDBNull(6) ? "" : query.GetString(6);
                        newPlanet.IdValue = query.GetInt32(7);
                        newPlanet.Icon = query.IsDBNull(8) ? null : (byte[])query[8];
                        planets.Add(newPlanet);
                    }
                    db.Close();
                }
                catch ( SQLiteException ex ) {
                    errorMessage = ex.Message;
                }
            }
            return planets;
        }

        /// <summary>
        /// Method for getting list of properties
        /// </summary>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>List of properties</returns>
        public List<Property> LoadProperties(out string errorMessage) {
            var properties = new List<Property>();
            errorMessage = "";
            using ( SQLiteConnection db = new SQLiteConnection(@"Data Source=.\Database\Planets.db") ) {
                db.Open();
                try {
                    string sqlQuery = @"select Properties.Id as Id, Properties.Name as Name FROM Properties";
                    properties = Database.SqlQuery<Property>(sqlQuery).ToList();
                    db.Close();
                }
                catch ( SQLiteException ex ) {
                    errorMessage = ex.Message;
                    db.Close();
                    return null;
                }
                return properties;
            }
        }

        /// <summary>
        /// Method for getting values which are connected with the property
        /// </summary>
        /// <param name="property">property for which the values are being searched</param>
        public bool LoadValuesForProperty(Property property, out string errorMessage) {
            property.PossibleValues = new List<Value>();
            errorMessage = "";
            using ( SQLiteConnection db = new SQLiteConnection(@"Data Source=.\Database\Planets.db") ) {
                db.Open();
                try {
                    string sqlQuery = String.Format(@"select Property_Values.Id as Id, Property_Values.Text_Value as Text FROM Property_Values
                                JOIN Pairs_Properties_Values as ppv ON ppv.Id_Property = @idProperty AND ppv.Id_Value = Id");
                    var values = Database.SqlQuery<Value>(sqlQuery, new SQLiteParameter("idProperty", property.Id)).ToList();
                    if ( values != null ) {
                        property.PossibleValues.AddRange(values);
                    }
                    db.Close();
                }
                catch ( SQLiteException ex ) {
                    errorMessage = ex.Message;
                    db.Close();
                    return false;
                }
                return true;
            }
        }

        public bool DeletePlanet(Planet deletedPlanet, out string errorMessage) {
            string sqlQuery = String.Format("Delete from Planets Where Id=@id", deletedPlanet.Id);
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("id", deletedPlanet.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        /// <summary>
        /// Method for deleting connectino between planet and property with value
        /// </summary>
        /// <param name="planet">planet for deleting property</param>
        /// <param name="deletedPair">pair property-value to be removed from planet properties</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>operation success</returns>
        public bool DeletePlanetProperty(Planet planet, PropertyValuePair deletedPair, out string errorMessage) {
            string sqlQuery = String.Format(@"Delete from Planets_Properties_Values 
						Where Id_Planet=@idPlanet and Id_Value=@idValue and Id_Property=@idProperty");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("idPlanet", planet.Id));
            parameters.Add(new SQLiteParameter("idValue", deletedPair.IdValue));
            parameters.Add(new SQLiteParameter("idProperty", deletedPair.IdProperty));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        public bool SavePlanet(Planet updatedPlanet, out string errorMessage) {
            string sqlQuery = String.Format("Update Planets SET Name = @name, Weight = @weight, Radius = @radius WHERE Id = @id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("name", updatedPlanet.Name));
            parameters.Add(new SQLiteParameter("weight", updatedPlanet.Weight));
            parameters.Add(new SQLiteParameter("radius", updatedPlanet.Radius));
            parameters.Add(new SQLiteParameter("id", updatedPlanet.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        /// <summary>
        /// Method for creating connection between planet and property with value
        /// </summary>
        /// <param name="planet">planet for adding property</param>
        /// <param name="propertyValue">pair property-value to be added to planet properties</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>operation success</returns>
        public bool AddPropertyValueToPlanet(Planet planet, PropertyValuePair propertyValue, out string errorMessage) {
            string sqlQuery = String.Format("Insert into Planets_Properties_Values (Id_Planet, Id_Property, Id_Value) values (@idPlanet, @idProperty, @idValue)");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("idValue", propertyValue.IdValue));
            parameters.Add(new SQLiteParameter("idPlanet", planet.Id));
            parameters.Add(new SQLiteParameter("idProperty", propertyValue.IdProperty));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        /// <summary>
        /// Method for updating connection between planet and property with value
        /// </summary>
        /// <param name="planet">planet for updating property</param>
        /// <param name="oldPropertyValue">pair property-value to be updated</param>
        /// <param name="newValue">new value to be added to property</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>operation success</returns>
        public bool UpdatePropertyValueToPlanet(Planet planet, PropertyValuePair oldPropertyValue, Value newValue, out string errorMessage) {
            string sqlQuery = String.Format("Update Planets_Properties_Values SET Id_Value = @idNewValue WHERE Id_Planet=@idplanet AND Id_Property=@idProperty AND Id_Value = @idOldValue", 
                newValue.Id, planet.Id, oldPropertyValue.IdProperty, oldPropertyValue.IdValue);
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("idNewValue", newValue.Id));
            parameters.Add(new SQLiteParameter("idPlanet", planet.Id));
            parameters.Add(new SQLiteParameter("idProperty", oldPropertyValue.IdProperty));
            parameters.Add(new SQLiteParameter("idOldValue", oldPropertyValue.IdValue));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }
        
        /// <summary>
        /// Method for getting all connected properties for one planet
        /// </summary>
        /// <param name="planet">planet with properties</param>
        /// <param name="errorMessage">parameter for storing error message if occurs</param>
        /// <returns>list of pairs property-value for planet</returns>
        public List<PropertyValuePair> LoadPropertiesAndValuesForPlanet(Planet planet, out string errorMessage) {
            List<PropertyValuePair> propertyValuePairs = new List<PropertyValuePair>();
            errorMessage = "";
            using ( SQLiteConnection db = new SQLiteConnection(@"Data Source=.\Database\Planets.db") ) {
                db.Open();
                try{
                    string sqlQuery = String.Format(@"SELECT  prop.Name as PropertyName, ppv.Id_Property as IdProperty,
												pv.Text_Value as ValueText, ppv.Id_Value as IdValue 
												from Planets_Properties_Values as ppv
												Left JOIN Properties as prop on prop.Id = ppv.Id_Property
												left join Property_Values as pv on pv.Id = ppv.Id_Value
												WHERE ppv.Id_Planet = @idPlanet");
                    var propertiesvalues = Database.SqlQuery<PropertyValuePair>(sqlQuery, new SQLiteParameter("idPlanet", planet.Id)).ToList();
                    if ( propertiesvalues != null ) {
                        propertyValuePairs = propertiesvalues;
                    }
                    db.Close();
                    return propertyValuePairs;
                }
                catch ( SQLiteException ex ) {
                    errorMessage = ex.Message;
                    db.Close();
                    return null;
                }
            }
        }

        public bool AddIconToPlanet(Byte[] icon, Planet planet, out string errorMessage) {
            string sqlQuery = String.Format("Update Planets SET Icon=@icon WHERE Id=@id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            SQLiteParameter iconParameter = new SQLiteParameter("icon", System.Data.DbType.Binary);
            iconParameter.Value = icon;
            parameters.Add(iconParameter);
            parameters.Add(new SQLiteParameter("id", planet.Id));
            return ExecuteQuery(sqlQuery, parameters, out errorMessage);
        }

        public bool DeleteIconForPlanet(Planet planet, out string errorMessage) {
            string sqlQuery = String.Format("Update Planets SET Icon=null WHERE Id=@id");
            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(new SQLiteParameter("id", planet.Id));
            bool success = ExecuteQuery(sqlQuery, parameters, out errorMessage);
            if ( success ) {
                planet.Icon = null;
            }
            return success;
        }

        /// <summary>
        /// Method for executing sql request and catching errors
        /// </summary>
        /// <param name="query">sql string</param>
        /// <param name="message">error message if occurs</param>
        /// <returns>operation success</returns>
        private bool ExecuteQuery(string query, List<SQLiteParameter> parameters, out string message) {
            using ( SQLiteConnection db = new SQLiteConnection(@"Data Source=.\Database\Planets.db") ) {
                db.Open();
                SQLiteTransaction transaction = db.BeginTransaction();
                SQLiteCommand command = new SQLiteCommand(query, db);
                command.Parameters.AddRange(parameters.ToArray());
                try {
                    command.ExecuteNonQuery();
                   // Database.ExecuteSqlCommand(query, parameters);
                    message = "";
                    transaction.Commit();
                    db.Close();
                    return true;
                }
                catch ( SQLiteException ex ) {
                    message = ex.Message;
                    transaction.Rollback();
                    db.Close();
                    return false;
                }
            }
        }

    }
}
