﻿using PlanetsDatabase.Database;
using PlanetsDatabase.Models;
using System.Collections.Generic;
using System.Windows;
using System;
using System.Windows.Controls;

namespace PlanetsDatabase
{
	/// <summary>
	/// Interaction logic for EditParameterWindow.xaml
	/// </summary>
	public partial class EditPropertyPlanetWindow : Window
	{

		public List<Value> Values { get; private set; }

        public Planet Planet;

		DatabaseContext dbContext;

		public Property CurrentProperty { get; private set; }
		public Value CurrentValue { get; private set; }

		public EditPropertyPlanetWindow( Planet planet, PropertyValuePair propertyValuePair) {
            InitializeComponent();
            CurrentProperty = propertyValuePair.GetPropertyFromPair();
            CurrentValue = propertyValuePair.GetValueFromPair();
            Planet = planet;
			dbContext = new DatabaseContext();
            GetValuesForProperty();
		}

		private void GetValuesForProperty()
		{
            PropertyName.Content = CurrentProperty.Name;
            if ( CurrentProperty != null )
			{
                string errorMessage = "";
                if (dbContext.LoadValuesForProperty(CurrentProperty, out errorMessage)) {
				    ValueComboBox.ItemsSource = CurrentProperty.PossibleValues;
                    ValueComboBox.SelectedItem = CurrentValue;
                }
                else {
                    MessageBox.Show("Can load values for property due to an error: " + errorMessage);
                }
			}
		}

        private void OkButton_Click(object sender, RoutedEventArgs e) {
            Value selectedValue = (Value)ValueComboBox.SelectedItem;

            if ( selectedValue != null ) {
                string errorMessage = "";
                if ( !dbContext.UpdatePropertyValueToPlanet(Planet, new PropertyValuePair(CurrentProperty, CurrentValue), selectedValue, out errorMessage) ) {
                    MessageBox.Show("Can not update value for property due to an error: " + errorMessage);
                }
                DialogResult = true;
                this.Close();
            }
            else {
                MessageBox.Show("Can not update property with empty value!");
            }
        }
	}
}
