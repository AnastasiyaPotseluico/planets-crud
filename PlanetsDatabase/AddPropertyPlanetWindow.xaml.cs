﻿using PlanetsDatabase.Database;
using PlanetsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlanetsDatabase
{
	/// <summary>
	/// Interaction logic for AddEditParameterWindow.xaml
	/// </summary>
	public partial class AddPropertyPlanetWindow : Window
	{
		public Planet Planet { get; private set; }

		public List<Property> Properties { get; private set; }

		DatabaseContext dbContext;

		public AddPropertyPlanetWindow(Planet planet)
		{
			InitializeComponent();
			dbContext = new DatabaseContext();
			Planet = planet;
			DataContext = Planet;
			GetPropertiesAndValues();
		}

		private void GetPropertiesAndValues()
		{
            string errorMessage = "";
            Properties = dbContext.LoadProperties(out errorMessage);
            if ( String.IsNullOrEmpty(errorMessage) ) {
                PropertyComboBox.ItemsSource = Properties;
                PropertyComboBox.SelectedIndex = 0;
            }
            else {
                MessageBox.Show("Can not load properties and values due to an error: " + errorMessage);
            }
		}

		private void PropertyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
            Property selectedProperty = (Property)PropertyComboBox.SelectedItem;
            string errorMessage = "";
			if (selectedProperty != null)
			{
                if ( dbContext.LoadValuesForProperty(selectedProperty, out errorMessage) ) {
                    ValueComboBox.ItemsSource = selectedProperty.PossibleValues;
                }
                else {
                    MessageBox.Show("Can not load values for property due to an error: " + errorMessage);
                }
			}
		}

        private void AddButton_Click(object sender, RoutedEventArgs e) {
            Property selectedProperty = (Property)PropertyComboBox.SelectedItem;
            Value selectedValue = (Value)ValueComboBox.SelectedItem;
            PropertyValuePair propVal = new PropertyValuePair(selectedProperty, selectedValue);
            if ( selectedProperty != null && selectedValue != null ) {
                string errorMessage = "";
                if ( !dbContext.AddPropertyValueToPlanet(Planet, propVal, out errorMessage) ) {
                    MessageBox.Show("Can not add property with value due to an error: " + errorMessage);
                }
                DialogResult = true;
                this.Close();
            }
            else if(selectedProperty == null){
                MessageBox.Show("Can not add empty property to the planet!");
            }
            else {
                MessageBox.Show("Can not add empty value to the planet!");
            }
        }
	}
}
