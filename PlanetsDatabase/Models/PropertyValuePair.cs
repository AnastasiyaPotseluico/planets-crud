﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlanetsDatabase.Models {
    /// <summary>
    /// Class for storing property-value pairs for planet
    /// </summary>
    public class PropertyValuePair : INotifyPropertyChanged {

        string _propertyName;
        string _valueText;

        public int IdProperty { get; set; }
        public int IdValue { get; set; }

        public string PropertyName {
            get {
                return _propertyName;
            }
            set {
                _propertyName = value;
                OnPropertyChanged("PropertyName");
            }
        }

        public string ValueText {
            get {
                return _valueText;
            }
            set {
                _valueText = value;
                OnPropertyChanged("ValueText");
            }
        }

        public PropertyValuePair() {
        }

        public PropertyValuePair(Property property, Value value) {
            IdProperty = property != null ? property.Id : -1;
            IdValue = value != null ? value.Id : -1;
            PropertyName = property != null ? property.Name : "";
            ValueText = value != null ? value.Text : "";
        }

        public Property GetPropertyFromPair() {
            Property property = new Property();
            property.Id = IdProperty;
            property.Name = PropertyName;
            return property;
        }

        public Value GetValueFromPair() {
            Value value = new Value();
            value.Id = IdValue;
            value.Text = ValueText;
            return value;
        }
         
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string property = "") {
            if ( PropertyChanged != null ) {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
