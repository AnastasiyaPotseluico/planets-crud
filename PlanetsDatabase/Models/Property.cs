﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlanetsDatabase.Models {

    /// <summary>
    /// Class for storing property data
    /// </summary>
    public class Property : INotifyPropertyChanged 
    {
        private string _name;
        private List<Value> _possibleValues; // List of values which can be assigned to this value

        public string Name {
            get {
                return _name;
            }
            set {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public List<Value> PossibleValues {
            get {
                return _possibleValues;
            }
            set {
                _possibleValues = value;
                OnPropertyChanged("PossibleValues");
            }
        }

        public Property() {
            Id = -1;
        }

        public bool Exists() {
            return Id > -1;
        }

        public int Id { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string property = "") {
            if ( PropertyChanged != null ) {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
