﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlanetsDatabase.Models
{
    /// <summary>
    /// Class for storing information about planet
    /// </summary>
	public class Planet : INotifyPropertyChanged
	{
		private string _name;

		private string _radius;

		private string _weight;

		public int Id { get; set; }

        // These fields were created specially for displaying properties with values in common table
		public int IdProperty { get; set; }
		public int IdValue { get; set; }
		public string ValueText { get; set; }
		public string PropertyName { get; set; }

        public byte[] Icon { get; set; }

        public Planet() {
            Id = -1;
        }

		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				OnPropertyChanged("Name");
			}
		}

		public string Radius
		{
			get { return _radius; }
			set
			{
				_radius = value;
				OnPropertyChanged("Radius");
			}
		}

		public string Weight
		{
			get { return _weight; }
			set
			{
				_weight = value;
				OnPropertyChanged("Weight");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged([CallerMemberName]string property = "")
		{
            if(PropertyChanged != null) {
			    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
		}

        public bool Exists() {
            return Id > -1;
        }
    }
}
