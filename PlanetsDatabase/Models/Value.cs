﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlanetsDatabase.Models {
    /// <summary>
    /// Class for storing value of property 
    /// </summary>
    public class Value : INotifyPropertyChanged {

        private string _text;

        public string Text {
            get {
                return _text;
            }
            set {
                _text = value;
                OnPropertyChanged("Text");
            }
        }
        public int Id { get; set; }

        public Value() {
            Id = -1;
        }

        public bool Exists() {
            return Id > -1;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string property = "") {
            if ( PropertyChanged != null ) {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
