# Planets CRUD Assignment
### Example of WPF CRUD application 
The application can be launched in two ways:  
1. the repository contains a file archive with an assembled application called ExecutableProgram.7z. This folder also contains the SQLite database Planets.db.  
2. clone the repository and build the application.  

The application allows you to add, edit and delete for planets, properties of planets and property values.  
As an example, two more text fields have been added to the planet - the radius and weight (it was decided to use the text, since the planets may differ in dimension by tens of orders of magnitude).  
The application allows you to filter the planets and properties by the value of any field (name, weight, property value, property name, planet name).